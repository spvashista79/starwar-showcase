import React, { Component } from 'react';
import { connect } from 'react-redux';
import {SearchInput,SearchInputResult  } from "../../components/search";
import {  getPlanetList } from '../../action-creators';
import {getPlanetDetails } from "../../actions";

class Search extends Component {
  componentDidMount() {
    this.props.getPlanetList();
  }

  getPlanetDetails = name => {
    this.props.getPlanetDetails(name);
  };

  render() {
    return (
      <>
        <SearchInput
            result={this.props.planetList}
            onChange={this.getPlanetDetails}
          />
          {this.props.planetDetails && this.props.planetDetails.length > 0 ? (
            <SearchInputResult result={this.props.planetDetails} />
          ) : null}
        
      </>
    );
  }
}

const mapStateToProps = state => ({
  planetList: state && state.planets &&state.planets.planetList,
  planetDetails: state && state.planets && state.planets.filteredPlanet
});

const mapDispatchToProps = dispatch => ({
  getPlanetList: () => dispatch(getPlanetList()),
  getPlanetDetails: name => dispatch(getPlanetDetails(name))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);


