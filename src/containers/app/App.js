import React from 'react';
import routes from './routes';
import { BrowserRouter as Router } from 'react-router-dom';
import './app.css';

const App=()=>{
  return (
    <div className="App">    
      <h2>Welcome to Star Wars API Demo</h2>
      <Router children={routes} />      
    </div>
  );
}

export default App;

