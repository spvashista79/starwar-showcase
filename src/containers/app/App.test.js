import React from 'react';
import { render } from '@testing-library/react';
import App from './app';

test('renders star wars text', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Star Wars/);
  expect(linkElement).toBeInTheDocument();
});

