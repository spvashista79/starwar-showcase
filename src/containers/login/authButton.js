import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { signOut } from '../../actions/index';

const AuthButton = props => {
  function signOut() {
    props.signOut();
    props.history.push('/');
  }

  return props.authenticated ? (
    <p >
      Welcome!{props.name}
      <button
        onClick={() => {
          signOut();
        }}
      >
        Sign out
      </button>
    </p>
  ) : null;
};

const mapStateToProps = state => ({
  authenticated: state && state.people && state.people.authenticated,
  name: state && state.people && state.people.name
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut())
});

const AuthButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AuthButton));
export default AuthButtonContainer;
