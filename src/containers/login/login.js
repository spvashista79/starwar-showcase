import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import LoginForm from '../../components/login/loginForm';
import { getPeople } from '../../action-creators/index';
import { checkAuth } from '../../actions/index';

class Login extends Component {
  componentDidMount() {
    this.props.getPeople();
  }

  authCheck = value => {
    this.props.checkAuth(value);
  };

  render() {
    if (this.props.authenticated) {
      return <Redirect to={'/search'} />;
    }
    return (
        <LoginForm onSubmit={this.authCheck} />      
    );
  }
}

const mapStateToProps = state => ({
  peopleDetail:state && state.people && state.people.peopleDetails,
  authenticated:state && state.people && state.people.authenticated,
});

const mapDispatchToProps = dispatch => ({
  getPeople: () => dispatch(getPeople()),
  checkAuth: value => dispatch(checkAuth(value))
});

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
export default LoginContainer;

