import React,{useState} from 'react'
import PropTypes from 'prop-types'

const SearchInput = ({ result, onChange }) =>{
  
   const [selectedVal,setSelectedVal]=useState(result[0] && result[0].name);

 
  const handleChange=(event)=>{
    setSelectedVal(event.target.value); 
    onChange(event.target.value);
  }

  let optionList = result.map((item, index) => <option key={index}>{item.name}</option>);


return( <section className="results-section">
     
<label>
     Select your planet :
     <select value={selectedVal} onChange={handleChange.bind(this)}>
      {optionList}
     </select>
   </label>
   
</section>)   
}
  
  SearchInput.propTypes = {
    onChange: PropTypes.func,
    result: PropTypes.array,
  }
  
  export default SearchInput