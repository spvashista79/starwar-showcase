import React from 'react'
import PropTypes from 'prop-types'

const SearchInputResult = ({ result }) => (
  <section className="results-section">

<div>
                    
            <div>
            <span>Name : </span>
            {result[0] && result[0].name}
            </div>
                            
            <div>
            <span>Climate : </span>
            {result[0] && result[0].climate}
            </div>

            <div>
            <span>Diameter : </span>
            {result[0] && result[0].diameter}
            </div>

            <div>
            <span>Population : </span>
            {result[0] && result[0].population}
            </div>

            <div>
            <span>Terrain : </span>
            {result[0] && result[0].terrain}
            </div>

            <div>
            <span>Gravity : </span>
            {result[0] && result[0].gravity}
            </div>


</div>
   
   
  </section>
)

SearchInputResult.propTypes = {
    result: PropTypes.array,
}

export default SearchInputResult
