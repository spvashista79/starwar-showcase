import React,{useState} from 'react';

const LoginForm=({onSubmit})=>{


const [inputName,setinputName]=useState('Luke Skywalker');

const [inputPasword,setinputPassword]=useState("19BBY");


const OnChangeName=(event)=>{
setinputName(event.target.value)
}


const OnChangePassword=(event)=>{
setinputPassword(event.target.value)
}

const signIn=()=>{
    onSubmit({username:inputName,password:inputPasword});
  }
 
  return (
    <div>
            <div>
            <label>
            <span>  Name:</span>
              <input type="text" value={inputName}  onChange={OnChangeName}  />
            </label>
            </div>
            <div>
            <label>  
            <span>  Password:</span>   
            </label>
            <input type="password" id="pwd" name="pwd" value={inputPasword}  onChange={OnChangePassword} />
            </div>
             <div>
               <button onClick={signIn}>
                Sign in
              </button>                               
              </div>      
         </div>   
    );

}

export default LoginForm;
export {LoginForm};

