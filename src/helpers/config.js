
export const config = {
    BASE_URL: '/starwar',
    API: {      
      DEV_URL: 'https://swapi.dev/api/',    
      PROD_URL: 'https://swapi.dev/api/', 
      GET_PEOPLE: 'people',
      GET_PLANETS: 'planets'      
    },
    PAGE_TITLES: {
      PEOPLE: 'people',
      PLANETS: 'planets',      
    },
    BREADCRUMB_PAGES: {
      PEOPLE: 0,
      PLANETS: 1,     
    },
    BREADCRUMB_LINKS: {
      PEOPLE: '/',
      PLANETS: '/planets',     
    },    
  };
  