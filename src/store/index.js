import { createStore, applyMiddleware } from 'redux';
import  thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers/index';

let store;
if (process.env.NODE_ENV === 'development') {
  const loggerMiddleware = createLogger();
  store = createStore(rootReducer,
                      applyMiddleware(thunkMiddleware, loggerMiddleware));
} else {
  store = createStore(rootReducer, applyMiddleware(thunkMiddleware));
}

export default store;
