import * as actionType from '../constants/action-type';

const defaultState = {
  authenticated: false,
  loading: true,
  peopleDetails:[],
};

const peopleReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionType.PEOPLE_REQUEST:
      return {
        ...state
      };

    case actionType.PEOPLE_RESPONSE:
      return {
        ...state,
        peopleDetails: action.payload,
        loading: false
      };

    case actionType.AUTH_CHECK:
      const { payload } = action;
      const result = state.peopleDetails.some(
        people =>
          people.name.toLowerCase() === payload.username.toLowerCase() &&
          people.birth_year === payload.password
      );
      return {
        ...state,
        authenticated: result
      };

    case actionType.SIGN_OUT:
      return {
        authenticated: false
      };

    default:
      return state;
  }
};

export default peopleReducer;
