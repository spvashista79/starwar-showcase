import * as actionType from '../constants/action-type';

const defaultState = {
  planetList: [],
  loading: true,
  filteredPlanet: []
};

const planetsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionType.PLANET_LIST_REQUEST:
      return {
        ...state
      };

    case actionType.PLANET_LIST_RESPONSE:
      return {
        ...state,
        planetList: action.payload,
        loading: false
      };
         
    case actionType.PLANET_DETAILS_REQUEST:
        return {
        ...state,
        filteredPlanet: state.planetList.filter(
          planet => planet.name.toLowerCase()=== action.payload.toLowerCase()
        )
      };

    default:
      return state;
  }
};

export default planetsReducer;
