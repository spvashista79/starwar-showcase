import { combineReducers } from 'redux';
import peopleReducer from './people';
import planetsReducer from './planets';

const rootReducer = combineReducers({
  authentication: false,
  people: peopleReducer,
  planets: planetsReducer
});

export default rootReducer;
