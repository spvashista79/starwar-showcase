import {
  PEOPLE_REQUEST,
  PEOPLE_RESPONSE,
  AUTH_CHECK,
  SIGN_OUT
} from '../constants/action-type';

export const peopleRequest = request => ({
  type: PEOPLE_REQUEST,
  payload: request });
  
export const peopleResponse = response => ({
  type: PEOPLE_RESPONSE,
  payload: response });


export const  checkAuth=(value) =>{
  return {
    type: AUTH_CHECK,
    payload: value
  };
}

export const signOut=() =>{
  return {
    type: SIGN_OUT
  };
}
