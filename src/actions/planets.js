import {
  PLANET_LIST_REQUEST,
  PLANET_LIST_RESPONSE,
  PLANET_DETAILS_REQUEST,
} from '../constants/action-type';


export const planetListRequest = request => ({
  type:PLANET_LIST_REQUEST,
  payload: request });
  

  export const planetListResponse = response => ({
    type:PLANET_LIST_RESPONSE,
    payload: response });


    export const getPlanetDetails = request => ({
      type:PLANET_DETAILS_REQUEST,
      payload: request });
          
    
