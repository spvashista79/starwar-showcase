import * as actiontype from './index'
import  * as constype from "../constants";
describe('actions test cases', () => {


    it('should create an action to people request', () => {
      
      const expectedAction = {
        type:constype.PEOPLE_REQUEST,
        payload: null }
  
      expect(actiontype.peopleRequest(null)).toEqual(expectedAction)
  
    });
    
    it('should create an action to people response', () => {
      
        const expectedAction = {
          type:constype.PEOPLE_RESPONSE,
          payload: null }
    
        expect(actiontype.peopleResponse(null)).toEqual(expectedAction)
    
      });

      it('should create an action to planets request', () => {
      
        const expectedAction = {
          type:constype.PLANET_LIST_REQUEST,
          payload: null }
    
        expect(actiontype.planetListRequest(null)).toEqual(expectedAction)
    
      });
      
      it('should create an action to planets response', () => {
        
          const expectedAction = {
            type:constype.PLANET_LIST_RESPONSE,
            payload: null }
      
          expect(actiontype.planetListResponse(null)).toEqual(expectedAction)
      
        });
  
  


});