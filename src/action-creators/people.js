import {
  peopleRequest,
   peopleResponse,  
} from '../actions/index';
import { api } from '../services'


export const getPeople=()=>{
  return dispatch => {
    dispatch(peopleRequest());
    return api.getPeople().then(res => {      
      dispatch(peopleResponse(res.data.results));
    });
  }
}
