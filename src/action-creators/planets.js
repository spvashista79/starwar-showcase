import {
  planetListRequest,
  planetListResponse,  
} from '../actions/index';
import { api } from '../services';


export const getPlanetList=()=>{
  return dispatch => {
    dispatch(planetListRequest());
    return api.getPlanetList().then(res => {      
      dispatch(planetListResponse(res.data.results));
    });
  }
}