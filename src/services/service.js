
 import axios from 'axios';
 import { validCodes } from '../helpers/utils.js';
 
 
 //===========Get Operations=====================//
 
 //Get Call
 const requestGetData=(url)=>{
   return axios(url)     
     .then(handleResponse)
     .catch(error=>console.log("Error in getting response",error)); 
      }


//callback of response (returns promise)
 const handleResponse=(response)=>{
    if (!validCodes(response.status)) {
    return Promise.reject(response);
  }
 
  return response;
}

export default requestGetData
