import requestGetData from '../services/service';
import {config} from '../helpers/config';


export const getPeople = () =>{
  let url=`${config.API.GET_PEOPLE}`;
  return requestGetData(url);   
}

export const getPlanetList = () =>{
  let url=`${config.API.GET_PLANETS}`;
  return requestGetData(url);
}
